package com.example.DelyAndFasty2.bean;

public class Platillo
{
    private Integer idplatillo;
    private Integer idbase;
    private Integer idacompa;
    private Integer idprincipal;
    private Integer idbebida;

    public Integer getIdplatillo() {
        return idplatillo;
    }

    public void setIdplatillo(Integer idplatillo) {
        this.idplatillo = idplatillo;
    }

    public Integer getIdbase() {
        return idbase;
    }

    public void setIdbase(Integer idbase) {
        this.idbase = idbase;
    }

    public Integer getIdacompa() {
        return idacompa;
    }

    public void setIdacompa(Integer idacompa) {
        this.idacompa = idacompa;
    }

    public Integer getIdprincipal() {
        return idprincipal;
    }

    public void setIdprincipal(Integer idprincipal) {
        this.idprincipal = idprincipal;
    }

    public Integer getIdbebida() {
        return idbebida;
    }

    public void setIdbebida(Integer idbebida) {
        this.idbebida = idbebida;
    }
}
