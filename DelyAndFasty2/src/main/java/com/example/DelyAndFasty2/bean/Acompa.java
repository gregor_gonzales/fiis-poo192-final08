package com.example.DelyAndFasty2.bean;

public class Acompa
{
    private Integer idacompa;
    private String nombre;
    private Float precio;

    public Integer getIdacompa() {
        return idacompa;
    }

    public void setIdacompa(Integer idacompa) {
        this.idacompa = idacompa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }
}
