package com.example.DelyAndFasty2.bean;

public class Base
{
    private Integer idbase;
    private String nombre;
    private Float precio;

    public Integer getIdbase() {
        return idbase;
    }

    public void setIdbase(Integer idbase) {
        this.idbase = idbase;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }
}
