package com.example.DelyAndFasty2.bean;

public class Principal
{
    private Integer idprincipal;
    private String nombre;
    private Float precio;;

    public Integer getIdprincipal() {
        return idprincipal;
    }

    public void setIdprincipal(Integer idprincipal) {
        this.idprincipal = idprincipal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }
}
