package com.example.DelyAndFasty2.Controlador;

import com.example.DelyAndFasty2.DAO.AcompaDAOImplement;
import com.example.DelyAndFasty2.bean.Acompa;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AcompaControlador
{
    @PostMapping("/RegistrarAcompa")
    public String RegistrarAcompa(@RequestParam String nombre, @RequestParam Float precio) throws Exception
    {
        Acompa tuc = new Acompa();
        AcompaDAOImplement tucdao = new AcompaDAOImplement();

        tuc.setNombre(nombre);
        tuc.setPrecio(precio);

        tucdao.RegistrarAcompa(tuc);
        return "Exito";
    }

    @PostMapping("/ActualizarAcompa")
    public String ActualizarAcompa(@RequestParam String nombre, @RequestParam Float precio, @RequestParam Integer idacompa) throws Exception
    {
        Acompa tuc = new Acompa();

        tuc.setNombre(nombre);
        tuc.setPrecio(precio);
        tuc.setIdacompa(idacompa);

        AcompaDAOImplement tucdao = new AcompaDAOImplement();
        tucdao.ActualizarAcompa(tuc);
        return "Exito";
    }

    @RequestMapping("/ListarAcompa")
    public List<Acompa> ListarAcompa() throws Exception
    {
        AcompaDAOImplement tucdao = new AcompaDAOImplement();
        return tucdao.ListarAcompa();
    }
}
