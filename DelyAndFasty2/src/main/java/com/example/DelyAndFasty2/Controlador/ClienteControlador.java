package com.example.DelyAndFasty2.Controlador;

import com.example.DelyAndFasty2.DAO.ClienteDAOImplement;
import com.example.DelyAndFasty2.bean.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class ClienteControlador
{
    @PostMapping("/RegistrarCliente")
    public String RegistrarCliente(@RequestParam String nombre, @RequestParam String apellido,
                                   @RequestParam Integer dni, @RequestParam String correo,
                                   @RequestParam String contrasena, @RequestParam String fecha_naci,
                                   @RequestParam Integer celular) throws Exception
    {
        Cliente cli = new Cliente();
        ClienteDAOImplement clidao = new ClienteDAOImplement();

        cli.setNombre(nombre);
        cli.setApellido(apellido);
        cli.setDni(dni);
        cli.setCorreo(correo);
        cli.setContrasena(contrasena);
        cli.setFecha_naci(fecha_naci);
        cli.setCelular(celular);

        clidao.RegistrarCliente(cli);
        return "<center>" +
                "   <h1>Cliente registrado con exito</h1>" +
                "   <h3>Bienvenido "+cli.getNombre()+" "+cli.getApellido()+"</h3>"+
                "   <button onclick=\"location.href='http://localhost:8088/Login.html'\">Iniciar sesion</button>" +
                "</center>";
    }

    @PostMapping("/ActualizarCliente")
    public String ActualizarCliente(@RequestParam Integer idclien, @RequestParam String nombre,
                                    @RequestParam String apellido, @RequestParam Integer dni,
                                    @RequestParam String correo, @RequestParam String contrasena,
                                    @RequestParam String fecha_naci, @RequestParam Integer celular) throws Exception
    {
        Cliente cli = new Cliente();

        cli.setIdcliente(idclien);
        cli.setNombre(nombre);
        cli.setApellido(apellido);
        cli.setDni(dni);
        cli.setCorreo(correo);
        cli.setContrasena(contrasena);
        cli.setFecha_naci(fecha_naci);
        cli.setCelular(celular);

        ClienteDAOImplement clidao = new ClienteDAOImplement();
        clidao.ActualizarCliente(cli);
        return "<center>" +
                "   <h1>Cliente actualizado con exito</h1>" +
                "   <button onclick=\"location.href='http://localhost:8088/Actualizar_Cliente.html'\">Actualizar otro cliente</button>" +
                "</center>";
    }

    /*@RequestMapping("/ListarCliente")
    public void ListarCliente(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        ClienteDAOImplement clidao = new ClienteDAOImplement();
        List<Cliente> lista = clidao.ListarCliente();
        request.setAttribute("lista",lista);
        request.getRequestDispatcher("/ListadoCliente.jsp").forward(request,response);
        //return clidao.ListarCliente();
    }*/

    @RequestMapping("/ListarCliente")
    public List<Cliente> ListarCliente() throws Exception
    {
        ClienteDAOImplement clidao = new ClienteDAOImplement();
        return clidao.ListarCliente();
    }
}
