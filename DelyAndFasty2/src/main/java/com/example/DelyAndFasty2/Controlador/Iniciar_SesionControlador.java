package com.example.DelyAndFasty2.Controlador;

import com.example.DelyAndFasty2.DAO.Iniciar_SesionDAOImplement;
import com.example.DelyAndFasty2.Request.IniciarSesionRequest;
import com.example.DelyAndFasty2.bean.Cliente;
import com.example.DelyAndFasty2.bean.Empleado;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Iniciar_SesionControlador
{
    @PostMapping("/IniciarSesion")
    public String IniciarSesion(@RequestParam String correo, @RequestParam String contrasena) throws Exception
    {
        Cliente cli = new Cliente();
        cli.setCorreo(correo);
        cli.setContrasena(contrasena);
        Empleado emp = new Empleado();
        emp.setCorreo(correo);
        emp.setContrasena(contrasena);
        Iniciar_SesionDAOImplement inidao =new Iniciar_SesionDAOImplement();
        //return inidao.IniciarSesion(cli,emp);
        return "ok";
    }

    @PostMapping("/IniciarSesion2")
    public String IniciarSesion2(@RequestBody IniciarSesionRequest request) throws Exception
    {
        Cliente cli = new Cliente();
        cli.setCorreo(request.getCorreo());
        cli.setContrasena(request.getContrasna());
        Empleado emp = new Empleado();
        emp.setCorreo(request.getCorreo());
        emp.setContrasena(request.getContrasna());
        Iniciar_SesionDAOImplement inidao =new Iniciar_SesionDAOImplement();
        return inidao.IniciarSesion(cli,emp);
    }
}
