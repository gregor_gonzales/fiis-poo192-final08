package com.example.DelyAndFasty2.Controlador;

import com.example.DelyAndFasty2.DAO.BebidaDAOImplement;
import com.example.DelyAndFasty2.bean.Bebida;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BebidaControlador
{
    @PostMapping("/RegistrarBebida")
    public String RegistrarBebida(@RequestParam String nombre, @RequestParam Float precio) throws Exception
    {
        Bebida tube = new Bebida();
        BebidaDAOImplement tubedao = new BebidaDAOImplement();

        tube.setNombre(nombre);
        tube.setPrecio(precio);

        tubedao.RegistrarBebida(tube);
        return "Exito";
    }

    @PostMapping("/ActualizarBebida")
    public String ActualizarBebida(@RequestParam String nombre, @RequestParam Float precio, @RequestParam Integer idbebida) throws Exception
    {
        Bebida tube = new Bebida();

        tube.setNombre(nombre);
        tube.setPrecio(precio);
        tube.setIdbebida(idbebida);

        BebidaDAOImplement tubedao = new BebidaDAOImplement();
        tubedao.ActualizarBebida(tube);
        return "Exito";
    }

    @RequestMapping("/ListarBebida")
    public List<Bebida> ListarBebida() throws Exception
    {
        BebidaDAOImplement tubedao = new BebidaDAOImplement();
        return tubedao.ListarBebida();
    }
}
