package com.example.DelyAndFasty2.Controlador;

import com.example.DelyAndFasty2.DAO.BaseDAOImplement;
import com.example.DelyAndFasty2.bean.Base;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BaseControlador
{
    @PostMapping("/RegistrarBase")
    public String RegistrarBase(@RequestParam String nombre, @RequestParam Float precio) throws Exception
    {
        Base tub = new Base();
        BaseDAOImplement tubdao = new BaseDAOImplement();

        tub.setNombre(nombre);
        tub.setPrecio(precio);

        tubdao.RegistrarBase(tub);
        return "Exito";
    }

    @PostMapping("/ActualizarBase")
    public String ActualizarBase(@RequestParam String nombre, @RequestParam Float precio, @RequestParam Integer idbase) throws Exception
    {
        Base tub = new Base();

        tub.setNombre(nombre);
        tub.setPrecio(precio);
        tub.setIdbase(idbase);

        BaseDAOImplement tubdao = new BaseDAOImplement();
        tubdao.ActualizarBase(tub);
        return "Exito";
    }

    @RequestMapping("/ListarBase")
    public List<Base> ListarBase() throws Exception
    {
        BaseDAOImplement tubdao = new BaseDAOImplement();
        return tubdao.ListarBase();
    }
}
