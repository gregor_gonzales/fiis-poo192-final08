package com.example.DelyAndFasty2.Controlador;

import com.example.DelyAndFasty2.DAO.PrincipalDAOImplement;
import com.example.DelyAndFasty2.bean.Principal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PrincipalControlador
{
    @PostMapping("/RegistrarLaPrincipal")
    public String RegistrarPrincipal(@RequestParam String nombre, @RequestParam Float precio) throws Exception
    {
        Principal lap = new Principal();
        PrincipalDAOImplement lapdao = new PrincipalDAOImplement();

        lap.setNombre(nombre);
        lap.setPrecio(precio);

        lapdao.RegistrarPrincipal(lap);
        return "Exito";
    }

    @PostMapping("/ActualizarLaPrincipal")
    public String ActualizarPrincipal(@RequestParam String nombre, @RequestParam Float precio, @RequestParam Integer idprincipal) throws Exception
    {
        Principal lap = new Principal();

        lap.setNombre(nombre);
        lap.setPrecio(precio);
        lap.setIdprincipal(idprincipal);

        PrincipalDAOImplement lapdao = new PrincipalDAOImplement();
        lapdao.ActualizarPrincipal(lap);
        return "Exito";
    }

    @RequestMapping("/ListarLaPrincipal")
    public List<Principal> ListarPrincipal() throws Exception
    {
        PrincipalDAOImplement lapdao = new PrincipalDAOImplement();
        return lapdao.ListarPrincipal();
    }
}
