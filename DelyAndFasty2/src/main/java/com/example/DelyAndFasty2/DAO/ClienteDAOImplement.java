package com.example.DelyAndFasty2.DAO;
import com.example.DelyAndFasty2.DAO.Interfaces.ClienteDAO;
import com.example.DelyAndFasty2.bean.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAOImplement extends Conexion implements ClienteDAO
{
    public void RegistrarCliente(Cliente cli) throws Exception
    {
        try
        {
            this.conectar();
            String sql = "insert into cliente (nombre,apellido,dni,correo,contrasena,fecha_naci,celular) values (?,?,?,?,?,?,?);";
            PreparedStatement pst = this.conexion.prepareStatement(sql);
            pst.setString(1,cli.getNombre());
            pst.setString(2,cli.getApellido());
            pst.setInt(3,cli.getDni());
            pst.setString(4,cli.getCorreo());
            pst.setString(5,cli.getContrasena());
            pst.setString(6,cli.getFecha_naci());
            pst.setInt(7,cli.getCelular());
            pst.executeUpdate();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
    }

    public void ActualizarCliente(Cliente cli) throws Exception
    {
        try
        {
            this.conectar();
            String sql = "update cliente set nombre = ? ,apellido = ? ,dni = ? ,correo = ? ,contrasena = ? ,fecha_naci = ? ,celular = ? where idclien = ?;";
            PreparedStatement pst = this.conexion.prepareStatement(sql);
            pst.setString(1,cli.getNombre());
            pst.setString(2,cli.getApellido());
            pst.setInt(3,cli.getDni());
            pst.setString(4,cli.getCorreo());
            pst.setString(5,cli.getContrasena());
            pst.setString(6,cli.getFecha_naci());
            pst.setInt(7,cli.getCelular());
            pst.setInt(8,cli.getIdcliente());
            pst.executeUpdate();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
    }

    public List<Cliente> ListarCliente() throws Exception
    {
        List<Cliente> lista = null;
        try
        {
            this.conectar();
            String sql = "select * from cliente";
            PreparedStatement pst = this.conexion.prepareStatement(sql);

            lista = new ArrayList();
            ResultSet rs = pst.executeQuery();
            while (rs.next())
            {
                Cliente cli = new Cliente();
                cli.setIdcliente(rs.getInt(1));
                cli.setNombre(rs.getString(2));
                cli.setApellido(rs.getString(3));
                cli.setDni(rs.getInt(4));
                cli.setCorreo(rs.getString(5));
                cli.setContrasena(rs.getString(6));
                cli.setFecha_naci(rs.getString(7));
                cli.setCelular(rs.getInt(8));

                lista.add(cli);
            }
            rs.close();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
        return lista;
    }
}
