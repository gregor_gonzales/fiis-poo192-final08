package com.example.DelyAndFasty2.DAO;

import com.example.DelyAndFasty2.DAO.Interfaces.Iniciar_SesionDAO;
import com.example.DelyAndFasty2.bean.Cliente;
import com.example.DelyAndFasty2.bean.Empleado;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Iniciar_SesionDAOImplement extends Conexion implements Iniciar_SesionDAO
{
    @Override
    public String IniciarSesion(Cliente cli, Empleado emp) throws Exception
    {
        try
        {
            this.conectar();
            String sql = "select * from cliente;";
            String sql2 = "select * from empleado;";
            PreparedStatement pst = this.conexion.prepareStatement(sql);
            ResultSet rst = pst.executeQuery();
            String a = "Usuario no registrado";
            while(rst.next())
            {
                String correo = rst.getString(5);
                String contrasena = rst.getString(6);
                if (cli.getCorreo().equals(correo))
                {
                    if (cli.getContrasena().equals(contrasena))
                    {
                        cli.setIdcliente(rst.getInt(1));
                        cli.setNombre(rst.getString(2));
                        cli.setApellido(rst.getString(3));
                        cli.setDni(rst.getInt(4));
                        cli.setCorreo(rst.getString(5));
                        cli.setContrasena(rst.getString(6));
                        cli.setFecha_naci(rst.getString(7));
                        cli.setCelular(rst.getInt(8));

                        a = "Bienvenido "+ cli.getNombre() + " " + cli.getApellido();
                    }
                    else
                    {
                        a= "Contraseña incorrecta";
                    }
                }
            }
            PreparedStatement ps = this.conexion.prepareStatement(sql2);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                String correo2 = rs.getString(5);
                String contrasena2 = rs.getString(6);
                if (emp.getCorreo().equals(correo2))
                {
                    if (emp.getContrasena().equals(contrasena2))
                    {
                        emp.setIdempleado(rs.getInt(1));
                        emp.setNombre(rs.getString(2));
                        emp.setApellido(rs.getString(3));
                        emp.setDni(rs.getInt(4));
                        emp.setCorreo(rs.getString(5));
                        emp.setContrasena(rs.getString(6));
                        emp.setCelular(rs.getInt(7));
                        emp.setIdcargo(rs.getInt(8));

                        if(emp.getIdcargo()==1)
                        {a = "Bienvenido "+ emp.getNombre() + " " + emp.getApellido() + ", mozo";}
                        if(emp.getIdcargo()==2)
                        {a = "Bienvenido "+ emp.getNombre() + " " + emp.getApellido() + ", administrador";}
                        if(emp.getIdcargo()==3)
                        {a = "Bienvenido "+ emp.getNombre() + " " + emp.getApellido() + ", repartidor";}
                    }
                    else
                    {
                        a= "Contraseña incorrecta";
                    }
                }
            }
            rs.close();
            ps.close();
            rst.close();
            pst.close();
            return a;
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
    }
}
