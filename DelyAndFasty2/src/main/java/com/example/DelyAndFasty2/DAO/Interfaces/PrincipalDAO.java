package com.example.DelyAndFasty2.DAO.Interfaces;

import com.example.DelyAndFasty2.bean.Principal;

import java.util.List;

public interface PrincipalDAO
{
    public void RegistrarPrincipal(Principal lap) throws Exception;
    public void ActualizarPrincipal(Principal lap) throws Exception;
    //public void EliminarLaPrincipal(Principal lap) throws Exception;
    public List<Principal> ListarPrincipal() throws Exception;
}

