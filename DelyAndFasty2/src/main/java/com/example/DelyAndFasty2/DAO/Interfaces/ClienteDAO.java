package com.example.DelyAndFasty2.DAO.Interfaces;
import com.example.DelyAndFasty2.bean.Cliente;

import java.util.List;

public interface ClienteDAO
{
    public void RegistrarCliente(Cliente cli) throws Exception;
    public void ActualizarCliente(Cliente cli) throws Exception;
    //public void EliminarCliente(Cliente cli) throws Exception;
    public List<Cliente> ListarCliente() throws Exception;
    //public String IniciarSesion(Cliente cli) throws Exception;
}
