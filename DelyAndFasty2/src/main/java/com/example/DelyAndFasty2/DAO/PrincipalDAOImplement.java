package com.example.DelyAndFasty2.DAO;

import com.example.DelyAndFasty2.DAO.Interfaces.PrincipalDAO;
import com.example.DelyAndFasty2.bean.Principal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class PrincipalDAOImplement extends Conexion implements PrincipalDAO
{
    public void RegistrarPrincipal(Principal lap) throws Exception
    {
        try
        {
            this.conectar();
            String sql = "insert into principal (nombre,nombre) values (?,?);";
            PreparedStatement pst = this.conexion.prepareStatement(sql);
            pst.setString(1,lap.getNombre());
            pst.setFloat(2,lap.getPrecio());
            pst.executeUpdate();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
    }

    public void ActualizarPrincipal(Principal lap) throws Exception
    {
        try
        {
            this.conectar();
            String sql = "update principal set nombre = ? ,nombre = ? where idprincipal = ?;";
            PreparedStatement pst = this.conexion.prepareStatement(sql);
            pst.setString(1,lap.getNombre());
            pst.setFloat(2,lap.getPrecio());
            pst.setInt(3,lap.getIdprincipal());
            pst.executeUpdate();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
    }

    /*public void EliminarLaPrincipal(La_Principal lap) throws Exception
    {

    }*/

    public List<Principal> ListarPrincipal() throws Exception
    {
        List<Principal> lista = null;
        try
        {
            this.conectar();
            String sql = "select * from principal";
            PreparedStatement pst = this.conexion.prepareStatement(sql);

            lista = new ArrayList();
            ResultSet rs = pst.executeQuery();
            while (rs.next())
            {
                Principal lap = new Principal();
                lap.setIdprincipal(rs.getInt(1));
                lap.setNombre(rs.getString(2));
                lap.setPrecio(rs.getFloat(3));

                lista.add(lap);
            }
            rs.close();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
        return lista;
    }
}
