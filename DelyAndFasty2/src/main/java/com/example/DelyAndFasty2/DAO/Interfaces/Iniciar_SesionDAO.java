package com.example.DelyAndFasty2.DAO.Interfaces;

import com.example.DelyAndFasty2.bean.Cliente;
import com.example.DelyAndFasty2.bean.Empleado;

public interface Iniciar_SesionDAO
{
    public String IniciarSesion(Cliente cli, Empleado emp) throws Exception;
}
