package com.example.DelyAndFasty2.DAO;

import com.example.DelyAndFasty2.DAO.Interfaces.BaseDAO;
import com.example.DelyAndFasty2.bean.Base;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class BaseDAOImplement extends Conexion implements BaseDAO
{
    public void RegistrarBase(Base tub) throws Exception
    {
        try
        {
            this.conectar();
            String sql = "insert into base (nombre,precio) values (?,?);";
            PreparedStatement pst = this.conexion.prepareStatement(sql);
            pst.setString(1,tub.getNombre());
            pst.setFloat(2, tub.getPrecio());
            pst.executeUpdate();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
    }

    public void ActualizarBase(Base tub) throws Exception
    {
        try
        {
            this.conectar();
            String sql = "update base set nombre = ? ,precio = ? where idbase = ?;";
            PreparedStatement pst = this.conexion.prepareStatement(sql);
            pst.setString(1,tub.getNombre());
            pst.setFloat(2, tub.getPrecio());
            pst.setInt(3,tub.getIdbase());
            pst.executeUpdate();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
    }

    public List<Base> ListarBase() throws Exception
    {
        List<Base> lista = null;
        try
        {
            this.conectar();
            String sql = "select * from base";
            PreparedStatement pst = this.conexion.prepareStatement(sql);

            lista = new ArrayList();
            ResultSet rs = pst.executeQuery();
            while (rs.next())
            {
                Base tub = new Base();
                tub.setIdbase(rs.getInt(1));
                tub.setNombre(rs.getString(2));
                tub.setPrecio(rs.getFloat(3));

                lista.add(tub);
            }
            rs.close();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
        return lista;
    }
}
