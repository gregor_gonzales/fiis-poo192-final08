package com.example.DelyAndFasty2.DAO.Interfaces;

import com.example.DelyAndFasty2.bean.Bebida;

import java.util.List;

public interface BebidaDAO
{
    public void RegistrarBebida(Bebida tube) throws Exception;
    public void ActualizarBebida(Bebida tube) throws Exception;
    //public void EliminarTuBebida(Tu_Bebida tube) throws Exception;
    public List<Bebida> ListarBebida() throws Exception;
}
