package com.example.DelyAndFasty2.DAO;

import com.example.DelyAndFasty2.DAO.Interfaces.BebidaDAO;
import com.example.DelyAndFasty2.bean.Bebida;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class BebidaDAOImplement extends Conexion implements BebidaDAO
{
    public void RegistrarBebida(Bebida tube) throws Exception
    {
        try
        {
            this.conectar();
            String sql = "insert into bebida (nombre,precio) values (?,?);";
            PreparedStatement pst = this.conexion.prepareStatement(sql);
            pst.setString(1,tube.getNombre());
            pst.setFloat(2,tube.getPrecio());
            pst.executeUpdate();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
    }

    public void ActualizarBebida(Bebida tube) throws Exception
    {
        try
        {
            this.conectar();
            String sql = "update bebida set nombre = ? ,precio = ? where idbebida = ?;";
            PreparedStatement pst = this.conexion.prepareStatement(sql);
            pst.setString(1,tube.getNombre());
            pst.setFloat(2,tube.getPrecio());
            pst.setInt(3,tube.getIdbebida());
            pst.executeUpdate();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
    }

    public List<Bebida> ListarBebida() throws Exception
    {
        List<Bebida> lista = null;
        try
        {
            this.conectar();
            String sql = "select * from bebida";
            PreparedStatement pst = this.conexion.prepareStatement(sql);

            lista = new ArrayList();
            ResultSet rs = pst.executeQuery();
            while (rs.next())
            {
                Bebida tube = new Bebida();
                tube.setIdbebida(rs.getInt(1));
                tube.setNombre(rs.getString(2));
                tube.setPrecio(rs.getFloat(3));

                lista.add(tube);
            }
            rs.close();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
        return lista;
    }
}
