package com.example.DelyAndFasty2.DAO.Interfaces;

import com.example.DelyAndFasty2.bean.Acompa;

import java.util.List;

public interface AcompaDAO
{
    public void RegistrarAcompa(Acompa tuc) throws Exception;
    public void ActualizarAcompa(Acompa tuc) throws Exception;
    //public void EliminarTusCompas(Tus_Compas tuc) throws Exception;
    public List<Acompa> ListarAcompa() throws Exception;
}
