package com.example.DelyAndFasty2.DAO.Interfaces;

import com.example.DelyAndFasty2.bean.Empleado;

import java.util.List;

public interface EmpleadoDAO
{
    public void RegistrarEmpleado(Empleado emp) throws Exception;
    public void ActualizarEmpleado(Empleado emp) throws Exception;
    //public void EliminarEmpleado(Empleado emp) throws Exception;
    public List<Empleado> ListarEmpleado() throws Exception;
    public String IniciarSesion(Empleado emp) throws Exception;
}
