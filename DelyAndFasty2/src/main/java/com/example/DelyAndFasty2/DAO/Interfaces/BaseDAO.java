package com.example.DelyAndFasty2.DAO.Interfaces;

import com.example.DelyAndFasty2.bean.Base;

import java.util.List;

public interface BaseDAO
{
    public void RegistrarBase(Base tub) throws Exception;
    public void ActualizarBase(Base tub) throws Exception;
    //public void EliminarTusBases(Tus_Bases tub) throws Exception;
    public List<Base> ListarBase() throws Exception;
}
