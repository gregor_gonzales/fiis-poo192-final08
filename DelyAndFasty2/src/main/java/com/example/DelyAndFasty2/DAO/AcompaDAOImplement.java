package com.example.DelyAndFasty2.DAO;

import com.example.DelyAndFasty2.DAO.Interfaces.AcompaDAO;
import com.example.DelyAndFasty2.bean.Acompa;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AcompaDAOImplement extends Conexion implements AcompaDAO
{
    public void RegistrarAcompa(Acompa tuc) throws Exception
    {
        try
        {
            this.conectar();
            String sql = "insert into acompa (nombre,precio) values (?,?);";
            PreparedStatement pst = this.conexion.prepareStatement(sql);
            pst.setString(1,tuc.getNombre());
            pst.setFloat(2,tuc.getPrecio());
            pst.executeUpdate();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
    }

    public void ActualizarAcompa(Acompa tuc) throws Exception
    {
        try
        {
            this.conectar();
            String sql = "update acompa set nombre = ? ,precio = ? where idcompas = ?;";
            PreparedStatement pst = this.conexion.prepareStatement(sql);
            pst.setString(1,tuc.getNombre());
            pst.setFloat(2,tuc.getPrecio());
            pst.setInt(3,tuc.getIdacompa());
            pst.executeUpdate();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
    }

    public List<Acompa> ListarAcompa() throws Exception
    {
        List<Acompa> lista = null;
        try
        {
            this.conectar();
            String sql = "select * from acompa";
            PreparedStatement pst = this.conexion.prepareStatement(sql);

            lista = new ArrayList();
            ResultSet rs = pst.executeQuery();
            while (rs.next())
            {
                Acompa tuc = new Acompa();
                tuc.setIdacompa(rs.getInt(1));
                tuc.setNombre(rs.getString(2));
                tuc.setPrecio(rs.getFloat(3));

                lista.add(tuc);
            }
            rs.close();
            pst.close();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.cerrar();
        }
        return lista;
    }
}
