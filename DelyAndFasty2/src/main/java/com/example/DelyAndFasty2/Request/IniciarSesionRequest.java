package com.example.DelyAndFasty2.Request;

public class IniciarSesionRequest
{
    private String correo;
    private String contrasna;

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasna() {
        return contrasna;
    }

    public void setContrasna(String contrasna) {
        this.contrasna = contrasna;
    }
}
