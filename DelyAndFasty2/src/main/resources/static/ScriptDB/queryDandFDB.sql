----------------------------------------------------------------------------------------------
----------------------para borrar tablas------------------------------------------------------
----------------------------------------------------------------------------------------------
drop table if exists orden;
drop table if exists pedido;
drop table if exists platillo;
drop table if exists base;
drop table if exists acompa;
drop table if exists principal;
drop table if exists bebida;
drop table if exists cliente;
drop table if exists empleado;
drop table if exists cargo;
----------------------------------------------------------------------------------------------
----------------------EMPEZAMOS A CREAR TABLAS------------------------------------------------
----------------------------------------------------------------------------------------------
create table cargo 
(
	idcargo  			serial 			not null,
	nombre  			text 			not null,
	primary key(idcargo)
);
-------------------------------------------------------------------------------------------
create table cliente 
(
	idcliente			serial 			not null,
	nombre				text 			not null,
	apellido			text 			not null,
	dni					integer 		not null,
	correo				text 			not null,
	contrasena			text 			not null,
	fecha_naci			text 			not null,
	celular				integer 		not null,
	unique(dni),
	unique(correo),
	unique(celular),
	primary key(idcliente)
);
--------------------------------------------------------------------------------------------------
create table empleado
(
	idempleado  		serial 			not null,
	nombre				text 			not null,
	apellido			text 			not null,
	dni					integer 		not null,
	correo				text 			not null,
	contrasena			text 			not null,
	celular  			integer 		not null,
	idcargo	  			integer 		not null,
	foreign key(idcargo) 		references cargo,
	primary key(idcargo,idempleado),
	unique (idempleado),	
	unique(dni),
	unique(correo),
	unique(celular)
);
------------------------------------------------------------------------------------------
create table base 
(
	idbase				serial 			not null,
	nombre				text 			not null,
	precio				numeric(5,2) 	not null,
	primary key(idbase)
);
--------------------------------------------------------------------------------------------
create table acompa
(
	idacompa			serial 			not null,
	nombre				text 			not null,
	precio				numeric(5,2) 	not null,
	primary key(idacompa)
);
------------------------------------------------------------------------------------------
create table principal 
(
	idprincipal			serial 			not null,
	nombre				text 			not null,
	precio				numeric(5,2) 	not null,
	primary key(idprincipal)
);
---------------------------------------------------------------------------------------------
create table bebida 
(
	idbebida			serial 			not null,
	nombre				text 			not null,
	precio				numeric(5,2) 	not null,
	primary key(idbebida)
);
----------------------------------------------------------------------------------------
create table platillo 
(
	idplatillo  		serial 			not null,
	idbase  			integer 		not null,
	idacompa  			integer 		not null,
	idprincipal  		integer 		not null,
	idbebida 			integer 		null,
	foreign key(idbase) 		references base,
	foreign key(idacompa) 		references acompa,
	foreign key(idprincipal) 	references principal,
	foreign key(idbebida) 		references bebida,
	primary key(idplatillo)
);
-------------------------------------------------------------------------------------------------------
create table pedido
(
	idpedido  			serial 			not null,
	metodopago			text 			not null,
	idcliente  			integer 		not null,
	idempleado			integer 		not null,
	foreign key(idcliente) 		references cliente(idcliente),
	foreign key(idempleado)		references empleado(idempleado),
	primary key(idpedido)
);
------------------------------------------------------------------------------------------------------
create table orden 
(
	idorden  			serial 			not null,
	idplatillo  		integer 		not null,
	idpedido  			integer 		not null,
	foreign key(idplatillo) 	references platillo(idplatillo),
	foreign key(idpedido) 		references pedido(idpedido),
	primary key(idorden)
);
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-----------------EMPEZAMOS A POBLAR LAS TABLAS---------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
insert into cargo (nombre) values ('mozo');
insert into cargo (nombre) values ('administrador');
insert into cargo (nombre) values ('respartidor');
-------------------------------------------
insert into cliente (nombre,apellido,dni,correo,contrasena,fecha_naci,celular)
	values ('cesar','atoche',12345678,'ceesaratoche@gmail.com','bandido','01/11/1999',987654321);
insert into cliente (nombre,apellido,dni,correo,contrasena,fecha_naci,celular)
	values ('ana','cuadros',12365478,'ana_cuadrosc@hotmail.com','siempre','17/04/1968',987635241);
insert into cliente (nombre,apellido,dni,correo,contrasena,fecha_naci,celular)
	values ('angelica','atoche',74561238,'angimel_19@gmail.com','angie','30/11/1986',968574321);
---------------------------------------------------------------------------------------------------------------------------------------------
insert into empleado (nombre,apellido,dni,correo,contrasena,celular,idcargo)
	values ('luis','coicca',1452637,'luisen17@hotmail.com','wonderfull',934707853,1);
insert into empleado (nombre,apellido,dni,correo,contrasena,celular,idcargo)
	values ('enrique','cuadros',1236547,'enrique16@gmail.com','cuadros',979876803,2);
insert into empleado (nombre,apellido,dni,correo,contrasena,celular,idcargo)
	values ('angel','perez',1536247,'angel09@gmaol.com','angelito',975548086,3);
---------------------------------------------------------------------------------------------------------------
insert into principal (nombre,precio) values ('saltado de pollo',3.5);
insert into principal (nombre,precio) values ('saltado de carne',4);
insert into principal (nombre,precio) values ('cerdo a la naranja',4);
insert into principal (nombre,precio) values ('tofu horneado',4);
insert into principal (nombre,precio) values ('nuggets c/ salsa bbq',4.5);
----------------------------------------------------------------
insert into bebida (nombre,precio) values ('inca kola',2);
insert into bebida (nombre,precio) values ('coca cola',2);
insert into bebida (nombre,precio) values ('cifrut',1.5);
insert into bebida (nombre,precio) values ('infusion',3);
------------------------------------------------
insert into base (nombre,precio) values ('arroz blanco',1);
insert into base (nombre,precio) values ('arroz chaufa',1.2);
insert into base (nombre,precio) values ('fideos tallarin',1.5);
insert into base (nombre,precio) values ('arroz integral',1.5);
---------------------------------------------------------
insert into acompa (nombre,precio) values ('fideo de arroz',0.5);
insert into acompa (nombre,precio) values ('fideo frito',0.5);
insert into acompa (nombre,precio) values ('champiñones',1);
insert into acompa (nombre,precio) values ('brocoli salteado',1);
----------------------------------------------------------
insert into platillo (idbase,idacompa,idprincipal,idbebida) values (1,3,2,1);
insert into platillo (idbase,idacompa,idprincipal,idbebida) values (2,2,4,3);
insert into platillo (idbase,idacompa,idprincipal,idbebida) values (3,1,3,2);
insert into platillo (idbase,idacompa,idprincipal,idbebida) values (4,4,1,4);
----------------------------------------------------
insert into pedido (metodopago,idcliente,idempleado) values ('efectivo',1,1);
insert into pedido (metodopago,idcliente,idempleado) values ('tarjeta',2,1);
insert into pedido (metodopago,idcliente,idempleado) values ('tarjeta',3,1);
------------------------------------------------------------
insert into orden (idplatillo,idpedido) values (1,1);
insert into orden (idplatillo,idpedido) values (4,1);
insert into orden (idplatillo,idpedido) values (3,2);

select count(*) from orden where idpedido=1;
select * from cliente;
select * from empleado;
select*from cargo;
