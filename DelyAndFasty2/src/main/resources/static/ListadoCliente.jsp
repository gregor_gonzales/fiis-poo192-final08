<%@page import="java.util.List"%>
<%@page import="com.example.DelyAndFasty2.bean.Cliente" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Listar Clientes</title>
    </head>

    <body>
    <div>
        <h3>Listado de Clientes...</h3>
        <p style="text-align: justify;">Aquí podras gestionar tus clientes registrados.</p>

        <br>
        <!---<a href="../../../java/com/example/DelyAndFasty2/Controlador/ClienteControlador.java?metodo=ListarCliente()"></a>--->
        <a href="ClienteControlador?metodo=ListarCliente"></a>
        <table class="">
            <tr>
                <th>Id</th> <th>Nombre</th> <th>Apellido</th>
                <th>DNI</th> <th>Correo</th> <th>Contraseña</th>
                <th>Fecha de nacimiento</th> <th>Teléfono</th>
            </tr>
            <%
                List<Cliente> lista = (List<Cliente>)request.getAttribute("lista");

                if(lista!=null)
                {
                    for(Cliente x:lista)
                    {
            %>
            <tr>
                <td> <%= x.getId_clien() %> </td>
                <td> <%= x.getNom_clien() %> </td>
                <td> <%= x.getApell_clien() %> </td>
                <td> <%= x.getDni_clien() %> </td>
                <td> <%= x.getCorreo_clien() %> </td>
                <td> <%= x.getContra_clien() %> </td>
                <td> <%= x.getFech_nac_clien() %> </td>
                <td> <%= x.getCel_clien() %> </td>


            </tr>
            <%
                    }
                }
            %>

        </table>
    </div>
    </body>
</html>