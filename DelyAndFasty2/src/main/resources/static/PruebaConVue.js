const app = new Vue({
    el: '#app',
    data: 
    {
        titulo: "Inventario",
        frutas: 
        [
            {
                // nombre: 'pera',
                // cantidad: 15
            }
        ],
        nuevafruta: '',
        nuevacantidad: null,
        Total: 0
    },
    methods: 
    {
        agregarfruta() 
        {
            this.frutas.push
            ({
                nombre: this.nuevafruta,
                cantidad: this.nuevacantidad
            });
            this.nuevafruta = '';
            this.nuevacantidad = null;
        }
    },
    computed: 
    {
        SumarFrutas() 
        {
            this.Total = 0;
            for (fru of this.frutas) 
            {
                this.Total = this.Total + fru.nuevacantidad;
            }
            return this.Total;
        }
    }
})